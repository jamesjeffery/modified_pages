<?php

declare(strict_types = 1);

namespace Drupal\Tests\modified_pages\Functional;

use Drupal\Tests\block\Functional\BlockTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Tests for the Modified Pages module.
 *
 * @group modified_pages
 */
class ModifiedPagesTests extends BlockTestBase {

  use NodeCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'filter',
    'test_page_test',
    'help',
    'block_test',
    'node',
    'modified_pages',
  ];

  protected function setUp() {
    parent::setUp();
    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'article',
        'name' => 'Article',
      ]);
    $type->save();

    $type = $this->container->get('entity_type.manager')->getStorage('node_type')
      ->create([
        'type' => 'page',
        'name' => 'Page',
      ]);
    $type->save();

    $this->container->get('router.builder')->rebuild();
  }

  public function testRestrictByType() {
    $node1 = $this->createNode(['type' => 'article']);
    $node2 = $this->createNode(['type' => 'page']);
    $block_name = 'todays_modified_pages';
    $title = $this->randomMachineName(8);
    $default_theme = $this->config('system.theme')->get('default');
    $edit = [
      'id' => strtolower($this->randomMachineName(8)),
      'region' => 'sidebar_first',
      'settings[label]' => $title,
      'settings[label_display]' => TRUE,
      'settings[types][page]' => 'page',
    ];
    // Set the block to list only "page" content.
    $edit['settings[types][page]'] = TRUE;
    $this->drupalGet('admin/structure/block/add/' . $block_name . '/' . $default_theme);
    $this->assertSession()->checkboxNotChecked('edit-settings-types-page');

    $this->drupalPostForm(NULL, $edit, t('Save block'));
    $this->assertSession()->pageTextContains('The block configuration has been saved.', 'Block was saved');

    $this->clickLink('Configure');
    $this->assertSession()->checkboxChecked('edit-settings-types-page');

    $this->drupalGet('');
    $this->assertSession()->pageTextNotContains($node1->getTitle(), 'Article content is not listed.');
    $this->assertSession()->pageTextContains($node2->getTitle(), 'Page content is listed.');
  }

}
