<?php

declare(strict_types = 1);

namespace Drupal\modified_pages\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a list of pages that were modified today.
 *
 * @Block(
 *   id = "todays_modified_pages",
 *   admin_label = @Translation("Today's modified pages"),
 *   category = @Translation("Modified Pages")
 * )
 */
class TodaysModifiedPages extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $timezone = new \DateTimeZone(drupal_get_user_timezone());
    $date = new \DateTime('today', $timezone);
    $query = \Drupal::entityQuery('node');
    $query->condition('changed', $date->getTimestamp(), '>=');

    if (!empty($config['types'])) {
      $query->condition('type', $config['types'], 'IN');
    }

    $query->sort('changed', 'DESC');
    $nids = $query->execute();

    if (empty($nids)) {
      return [
        '#markup' => $this->t('There is no recently modified content.'),
      ];
    }
    else {
      $items = [];
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);
      foreach ($nodes as $node) {
        $items[] = $node->toLink()->toRenderable();
      }

      return [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $items,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $form = parent::blockForm($form, $form_state);

    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($types as $type) {
      $options[$type->id()] = $type->label();
    }
    $form['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#description' => $this->t("Select the content types to list today's modified content for. If none are selected, then all types will be included."),
      '#default_value' => isset($config['types']) ? $config['types'] : [],
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['types'] = array_filter($form_state->getValue('types'));
  }

  public function getCacheMaxAge() {
    return 0;
  }

}
